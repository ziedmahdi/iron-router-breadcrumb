String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

if (Meteor.isClient) {
    Breadcrumb = {
        getBreadcrumbObject: function (route) {
            // replace all parameters in the title
            var title = route.options.hasOwnProperty('title') ? route.options.title : Router.options.defaultBreadcrumbTitle;

            if ('function' === typeof title) {
                title = _.bind(title, Router.current())();
            }

            if (!route.options.noCaps) {
                title = title.capitalize();
            }

            var isCurrent = Router.current().route.options.name == route.options.name;

            var cssClasses = isCurrent ? 'active' : '';

            var showLink = true;
            if (route.options.showLink) {
                showLink = route.options.showLink;
            } else {
                if (typeof Router.options.defaultBreadcrumbLastLink !== 'undefined' && isCurrent) {
                    showLink = Router.options.defaultBreadcrumbLastLink;
                }
            }


            return {
                path: route.options.name,
                title: title,
                showLink: showLink,
                cssClasses: cssClasses,
                url: route.path(Router.current().params),
                route: route
            };
        },

        getBreadcrumbElementsForRoute: function (route) {
            if (route) {
                var current = route.options.name;
                var parent = route.options.hasOwnProperty('parent') ? route.options.parent : Router.options.parent;

                if ('function' === typeof parent) {
                    parent = _.bind(parent, Router.current())();
                }

                if (parent) {
                    return [this.getBreadcrumbObject(route)].concat(this.getBreadcrumbElementsForRoute(Router.routes[parent]));
                } else {
                    return [this.getBreadcrumbObject(route)];
                }
            } else {
                // no route have been specified
                return [];
            }
        }
    };

    Template.registerHelper('Breadcrumb', function (template) {
        return _.compact(Breadcrumb.getBreadcrumbElementsForRoute(Router.current().route)).reverse();
    });
}