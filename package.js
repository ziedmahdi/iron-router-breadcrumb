Package.describe({
  name: 'ziedmahdi:iron-router-breadcrumb',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Easy way to add a breadcrumb to Iron.Router with enough flexibility',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/ziedmahdi/iron-router-breadcrumb',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: null
});

Package.onUse(function(api) {
  api.versionsFrom('0.9.0');
  api.use(
      [
        'blaze@2.0.0',
        'templating@1.0.5',
        'underscore@1.0.4'
      ]
  );

  api.use('iron:router@1.0.1', 'client');

  api.addFiles('iron-router-breadcrumb.js');
  api.addFiles('iron-router-breadcrumb.html');

  api.export('Breadcrumb');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('ziedmahdi:iron-router-breadcrumb');
  api.addFiles('iron-router-breadcrumb-tests.js');
});
